import rclpy
from rclpy.node import Node
from std_msgs.msg import Float64MultiArray
from sensor_msgs.msg import Joy
from helper_functions.gamepad_functions import GamepadFunctions
from rcl_interfaces.msg import ParameterDescriptor
from .playback_functions import PlaybackHand
from copy import deepcopy as cp
from helper_functions.utility import y_n_prompt

class HandPlaybackNode(Node):

    def __init__(self):
        super().__init__('playback_node')

        self.gp = GamepadFunctions()
        self.play_hand = PlaybackHand()

        # Publisher
        self.tendon_pos_publisher = self.create_publisher(Float64MultiArray, '/tendon_position_demand', 10)

        # Subscriber
        self.mainloop = self.create_timer(0.001, self.mainloop_callback) 
        self.gamepad_subscriber = self.create_subscription(Joy, '/gamepad', self.gamepad_callback, 10)
        self.tendon_position_subscriber = self.create_subscription(Float64MultiArray, '/hand_controller/tendon_positions', self.tendon_position_callback, 10)

        # Initial values
        self.tendon_position = None  

        self.print_menu()

    def print_menu(self):
        print("\n---\nPress 'circle' for playback\n---\n")

    def tendon_position_callback(self, tendon_position:Float64MultiArray):
        self.tendon_position = tendon_position

    def gamepad_callback(self, gamepad_raw_msg:Joy):
        self.gp.convert_joy_msg_to_dictionary(gamepad_raw_msg)

    def mainloop_callback(self):
        if self.tendon_position is not None:
            if self.gp.if_button_pressed("circle"):
                self.play_hand.select_and_playback(self.tendon_position, self.tendon_pos_publisher)
                self.print_menu()

def main(args=None):
    rclpy.init(args=args)

    hand_playback_node = HandPlaybackNode()

    rclpy.spin(hand_playback_node)

    hand_playback_node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()