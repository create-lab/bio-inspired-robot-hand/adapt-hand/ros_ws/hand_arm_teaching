import rclpy
from rclpy.node import Node
from std_msgs.msg import Float64MultiArray
from sensor_msgs.msg import Joy
from helper_functions.gamepad_functions import GamepadFunctions
from .teaching_functions import TeachHand

class HandTeachingNode(Node):

    def __init__(self):
        super().__init__('hand_teaching_node')

        self.gp = GamepadFunctions()
        self.teach_hand = TeachHand()

        # Subscriber
        self.mainloop = self.create_timer(0.001, self.mainloop_callback) 
        self.gamepad_subscriber = self.create_subscription(Joy, '/gamepad', self.gamepad_callback, 10)
        self.tendon_position_subscriber = self.create_subscription(Float64MultiArray, '/hand_controller/tendon_positions', self.tendon_position_callback, 10)

        # Initialize variables
        self.tendon_position = None
        self.first_wp = True
        
    def print_menu(self):
        print("\n---\nPress 'circle' to record waypoint\nPress 'square' to save file\n---\n")

    def tendon_position_callback(self, tendon_position:Float64MultiArray):
        self.tendon_position = tendon_position

    def gamepad_callback(self, gamepad_raw_msg:Joy):
        self.gp.convert_joy_msg_to_dictionary(gamepad_raw_msg)
        # self.get_logger().info('I heard: "%s"' % "gamepad", throttle_duration_sec = 0.5)

    def mainloop_callback(self):
        if self.tendon_position is not None:
            # self.get_logger().info('I heard: "%s"' % self.gp.button_data, throttle_duration_sec = 1)

            if self.first_wp:
                self.teach_hand.add_waypoint(self.tendon_position)
                self.first_wp = False
                self.print_menu()
                
            if self.gp.if_button_pressed("circle"):
                print("circle")
                self.get_logger().info("\nAdding HAND waypoint", throttle_duration_sec = 0.01)  
                self.teach_hand.add_waypoint(self.tendon_position)
                self.print_menu()
            
            if self.gp.if_button_pressed("square"):
                self.get_logger().info("\nSaving HAND waypoint file", throttle_duration_sec = 0.01)  

                csv_name = input("\n\nType file name and press enter: ")              
                self.teach_hand.write_hand_waypoints_to_file(csv_name)
                self.print_menu()


def main(args=None):
    rclpy.init(args=args)

    hand_teaching_node = HandTeachingNode()

    rclpy.spin(hand_teaching_node)

    hand_teaching_node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()