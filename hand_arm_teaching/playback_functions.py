import os
import sys
import json
import time
import csv
import numpy as np
from std_msgs.msg import Float64MultiArray
from rclpy.publisher import Publisher
from helper_functions.utility import y_n_prompt
from helper_functions.gamepad_functions import GamepadFunctions
from copy import deepcopy as cp
import roslibpy
import numpy as np

class BasePlaybackFunctions:
    def __init__(self) -> None:
        current_directory = os.path.dirname(os.path.realpath(__file__))
        self.parent_dir = current_directory + "/teaching/"

    def set_file_directory(self, dir_name):
        self.dir_name = dir_name

    def _load_csv_to_array(self, csv_name):
        csv_file = self.parent_dir + self.dir_name + "/" + csv_name + ".csv"
        wp = []
        with open(csv_file, 'r') as file:
            csvreader = csv.reader(file)
            for row in csvreader:
                row2 = []
                for number in row:
                    row2.append(float(number))
                wp.append(row2)
        return wp
    
    def _linear_interpolation(self, start, end, division, num_of_division):
        demand = []
        for i in range(len(start)):
            diff = end[i] - start[i]
            division_ratio = division/num_of_division
            demand.append((diff * division_ratio) + start[i])
        return demand

    def _play_trajectory(self, saved_wp, initial_pose:Float64MultiArray, publisher:Publisher, get_velocity_params, 
                        vel = 0.1, interpolation_correction = None, revert_interpolation_correction = None, is_roslibpy = False):
        
        if is_roslibpy:
            initial_pose = [list(initial_pose["data"])]
        else:    
            initial_pose = [list(initial_pose.data)]


        wp = initial_pose + saved_wp

        for i in range(len(wp) - 1):
            current_pose = wp[i]
            next_pose = wp[i+1]

            # Corrections for neagtive crossings in Panda arm
            if interpolation_correction is not None:
                next_pose, track_corrections = interpolation_correction(current_pose, next_pose)

            dt, num_of_divisions = get_velocity_params(current_pose, next_pose, target_vel = vel)

            for div in range(num_of_divisions):
                demand_temp = self._linear_interpolation(current_pose, next_pose, div, num_of_divisions)

                # Uncorrect negative crossings in panda arm
                if revert_interpolation_correction is not None:
                    demand_temp = revert_interpolation_correction(demand_temp, track_corrections)
                
                if is_roslibpy:
                    publisher.publish(roslibpy.Message({'data': demand_temp}))
                else:
                    demand = Float64MultiArray()
                    demand.data = demand_temp
                    publisher.publish(demand)
                
                time.sleep(dt)

class PlaybackArm(BasePlaybackFunctions):

    def __init__(self) -> None:
        super().__init__()
        self.pose_offset = [0, 0, 0, 0, 0, 0]

    # Public
    def get_command_json_file(self):
        return json.load(open(self.parent_dir + self.dir_name + "/" + 
                                self.dir_name + ".json"))[self.dir_name]

    def correct_negative_crossing(self, start_pose, goal_pose):
        new_goal_pose = cp(goal_pose)
        track_negative_crossing = [False, False, False]

        # Loop through start/goal angle pairs
        for i in range(3,6):
            condition_1 = abs(start_pose[i]) > np.pi/2
            condition_2 = abs(goal_pose[i]) > np.pi/2
            condition_3 = np.sign(start_pose[i]) != np.sign(goal_pose[i])

            # The angles are wrong
            if condition_1 and condition_2 and condition_3:
                new_goal_pose[i] = goal_pose[i] - np.sign(goal_pose[i]) * 2 * np.pi
                track_negative_crossing[i-3] = True

        return new_goal_pose, track_negative_crossing

    def revert_negative_corrsing_correction(self, demand, track_corrections):
        for i, is_correction_needed in enumerate(track_corrections):
                if abs(demand[i+3]) > np.pi:
                    demand[i+3] = demand[i+3] - (np.sign(demand[i+3]) * 2 * np.pi) * is_correction_needed

        return demand

    def compute_arm_vel_params(self, start_pose, goal_pose, target_vel = 0.1, minimum_time = 0.5, dt = 0.01):
        translation_dist = np.linalg.norm(np.asarray(start_pose[0:3]) - np.asarray(goal_pose[0:3]))
        rotation_dist = np.linalg.norm(np.asarray(start_pose[3:6]) - np.asarray(goal_pose[3:6]))
        translation_weight = 0.8
        rotation_scaling = 0.3
        dist = translation_dist * translation_weight + rotation_dist * (1-translation_weight) * rotation_scaling

        t = max(dist/target_vel, minimum_time)
        t = round(t, 2)

        num_of_divisions = round(t/dt)

        return dt, num_of_divisions

    def apply_offset_to_waypoints(self, waypoints):
        for waypoint in waypoints:
            for i in range(6):
                waypoint[i] += self.pose_offset[i]

        return waypoints
    
    def play_arm_trajectory(self, cmd, initial_franka_pose:Float64MultiArray, franka_euler_demand_publisher:Publisher, default_vel=0.1):
        wp = self._load_csv_to_array(cmd["csv_name"])
        wp = self.apply_offset_to_waypoints(wp)

        vel = default_vel
        if "velocity" in cmd.keys():
            vel = cmd["velocity"] 

        self._play_trajectory(wp, initial_franka_pose, franka_euler_demand_publisher, self.compute_arm_vel_params, vel=vel,
                              interpolation_correction=self.correct_negative_crossing,
                              revert_interpolation_correction=self.revert_negative_corrsing_correction)
        
    def apply_high_level_settings(self, cmd:dict, franka_stiffness_publisher:Publisher):
        if "stiffness" in cmd.keys():
            demand_stiffness = Float64MultiArray()
            demand_stiffness.data = cmd["stiffness"]
            franka_stiffness_publisher.publish(demand_stiffness)
            print("\nStiffness updated to:", cmd["stiffness"], "\n")

        if "offset" in cmd.keys():
            self.pose_offset = cmd["offset"]
            print("\nOffset updated to:", cmd["offset"], "\n")


class PlaybackHand(BasePlaybackFunctions):

    def __init__(self) -> None:
        super().__init__()
        self.set_file_directory("hand")

    def compute_hand_vel_params(self, start_pose, end_pose, target_vel = 2, minimum_vel = 0.1, dt = 0.01):
        
        max_tendon_displacement = -1
        for i in range(len(start_pose)):
            tendon_displacement = abs(end_pose[i] - start_pose[i])
            if tendon_displacement > max_tendon_displacement:
                max_tendon_displacement = tendon_displacement

        t = max(max_tendon_displacement/target_vel, minimum_vel)
        t = round(t, 2)

        num_of_divisions = round(t/dt)

        return dt, num_of_divisions
    
    def write_torque_at_end_to_wp(self, wp, torque_at_end, multiplier = 2):
        wp_copy = cp(wp)
        final_wp = cp(wp[-1])

        extra_torque_mapping = [0, 2, 4, 6, 8]
        for tq, index in zip(torque_at_end, extra_torque_mapping):
            final_wp[index] += tq * multiplier
    
        return wp_copy + [final_wp]
    
    def write_extra_torque_to_wp(self, waypoints, extra_torque, multiplier = 2):

        extra_torque_mapping = [0, 2, 4, 6, 8]
        for waypoint in waypoints:
            for torque, index in zip(extra_torque, extra_torque_mapping):
                waypoint[index] += torque * multiplier

        return waypoints
                
    def playback_hand(self, file_name, initial_tendon_pos:Float64MultiArray, 
                      tendon_demand_publisher:Publisher, reverse_wp = False, vel = 4, extra_torque = None, torque_at_end = None, is_roslibpy = False):
        wp = self._load_csv_to_array(file_name)

        if extra_torque is not None:
            wp = self.write_extra_torque_to_wp(wp, extra_torque)

        if torque_at_end is not None:
            wp = self.write_torque_at_end_to_wp(wp, torque_at_end)

        if reverse_wp:
            wp.reverse()

        self._play_trajectory(wp, initial_tendon_pos, tendon_demand_publisher, self.compute_hand_vel_params, vel = vel, is_roslibpy=is_roslibpy)

    def list_and_select_file_name(self):
        files = os.listdir(self.parent_dir + self.dir_name)

        print("\n0 : Exit")
        for i, file in enumerate(files):
            print(i+1, ":", file[:-4])
        
        select = input("\nChoose which file to play (write number and press enter): ")     
        try:
            select = int(select)
        except:
            return None
        
        if select != 0:
            file_name = files[select -1][:-4]
        else:
            return None

        if y_n_prompt("\nIs this action in reverse? (y/n):"):
            reverse = True
        else:
            reverse = False  
        
        return file_name, reverse

    def select_and_playback(self, initial_tendon_pos:Float64MultiArray, tendon_demand_publisher:Publisher):
        contents = self.list_and_select_file_name()

        if contents is None:
            print("No file is selected")
        else:
            self.playback_hand(contents[0], initial_tendon_pos, tendon_demand_publisher, contents[1])

    def playback_with_options(self, cmd:dict, initial_tendon_pos:Float64MultiArray, tendon_demand_publisher:Publisher, default_vel):
        extra_torque = None
        torque_at_end = None
        vel = default_vel

        if "extra torque" in cmd.keys():
            extra_torque = cmd["extra torque"]
        
        if "torque at end" in cmd.keys():
            torque_at_end = cmd["torque at end"]

        if "velocity" in cmd.keys():
            vel = cmd["velocity"]

        self.playback_hand(cmd["csv_name"], initial_tendon_pos, tendon_demand_publisher, cmd["reverse"], vel = vel, 
                                        extra_torque = extra_torque, torque_at_end = torque_at_end)
        time.sleep(0.5)