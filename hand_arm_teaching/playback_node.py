import rclpy
from rclpy.node import Node
from std_msgs.msg import Float64MultiArray
from sensor_msgs.msg import Joy
from helper_functions.gamepad_functions import GamepadFunctions
from rcl_interfaces.msg import ParameterDescriptor
from .playback_functions import PlaybackArm, PlaybackHand
from copy import deepcopy as cp
import sys
import musicalbeeps
import time

class PlaybackNode(Node):

    def __init__(self):
        super().__init__('playback_node')

        self.gp = GamepadFunctions()
        self.play_arm = PlaybackArm()
        self.play_hand = PlaybackHand()

        # Parameters
        param_descriptor = ParameterDescriptor(description='Folder name for the trajectory')
        self.declare_parameter('rec_name', 'test', param_descriptor)

        # Publisher
        self.franka_euler_demand_publisher = self.create_publisher(Float64MultiArray, '/franka/euler_demand', 10)
        self.franka_stiffness_demand_publisher = self.create_publisher(Float64MultiArray, '/franka/stiffness_demand', 10)
        self.tendon_pos_publisher = self.create_publisher(Float64MultiArray, '/tendon_position_demand', 10)

        # Subscriber
        self.mainloop = self.create_timer(0.001, self.mainloop_callback) 
        self.gamepad_subscriber = self.create_subscription(Joy, '/gamepad', self.gamepad_callback, 10)
        self.franka_euler_pose_subscriber = self.create_subscription(Float64MultiArray, '/franka/euler_pose', self.franka_euler_pose_callback, 10)
        self.tendon_position_subscriber = self.create_subscription(Float64MultiArray, '/hand_controller/tendon_positions', self.tendon_position_callback, 10)

        selected_file_name = self.get_parameter('rec_name').value
        self.play_arm.set_file_directory(selected_file_name)
        self.commands = self.play_arm.get_command_json_file()

        self.number_of_commands = len(self.commands)
        self.next_command_num = 0

        demand_stiffness = Float64MultiArray()
        demand_stiffness.data = [400.0, 400.0, 400.0, 30.0, 30.0, 30.0, 0.0]
        self.franka_stiffness_demand_publisher.publish(demand_stiffness)

        # Music
        self.beeper = musicalbeeps.Player(volume = 1,
                            mute_output = True)
        
        # Initial values
        self.tendon_position = None
        self.franka_euler_pose = None
        self.is_ready = False

        # Instructions
        print("\nSelected file:", selected_file_name)
        print("\nLoading ...")

    def tendon_position_callback(self, tendon_position:Float64MultiArray):
        self.tendon_position = tendon_position

    def franka_euler_pose_callback(self, franka_euler_pose:Float64MultiArray):
        self.franka_euler_pose = franka_euler_pose

    def gamepad_callback(self, gamepad_raw_msg:Joy):
        self.gp.convert_joy_msg_to_dictionary(gamepad_raw_msg)

    def mainloop_callback(self):
        if (self.franka_euler_pose is not None) and (self.tendon_position is not None) and (self.is_ready == False):
            self.is_ready = True
            print("\n---\nPress 'circle': start playback\n---\n")
            self.beeper.play_note("A", 0.5)

        # if self.is_ready and self.gp.if_button_pressed("circle"):
        if self.gp.button_data["R1"] == 1:
            self.get_logger().info("\nGetting commands json\n", throttle_duration_sec = 0.01)

            # Next command
            cmd = self.commands[self.next_command_num]

            if cmd["type"] == "arm":
                self.play_arm.play_arm_trajectory(cmd, self.franka_euler_pose, 
                                                    self.franka_euler_demand_publisher, default_vel = 0.07)
                
            elif cmd["type"] == "hand":
                self.play_hand.playback_with_options(cmd, self.tendon_position, self.tendon_pos_publisher, default_vel = 8)
                
            elif cmd["type"] == "arm setting":
                self.play_arm.apply_high_level_settings(cmd, self.franka_stiffness_demand_publisher)

            self.next_command_num += 1
            if self.next_command_num < self.number_of_commands:
                # self.beeper.play_note("A", 0.2)
                pass
            else:
                print("End of commands")
                self.beeper.play_note("C", 0.5)
                sys.exit()

def main(args=None):
    rclpy.init(args=args)

    playback_node = PlaybackNode()

    rclpy.spin(playback_node)

    playback_node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()