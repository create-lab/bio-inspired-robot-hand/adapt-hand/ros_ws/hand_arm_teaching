import os
import sys
import json
import numpy as np
from std_msgs.msg import Float64MultiArray
import shutil
import csv
from helper_functions.utility import y_n_prompt
from helper_functions.gamepad_functions import GamepadFunctions
from copy import deepcopy as cp

class BaseTeachingFunctions:
    def __init__(self) -> None:
        self.pose_list = []
        
        current_directory = os.path.dirname(os.path.realpath(__file__))
        self.parent_dir = current_directory + "/teaching/"

    def _set_dir_name(self, dir_name):
        self.dir_name = dir_name

    def _write_csv(self, csv_name, csv_content:list):
        csv_file = self.parent_dir + self.dir_name + "/" + csv_name + ".csv"
        with open(csv_file, 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerows(csv_content)

    def _save_waypoint_csv(self, csv_name):
        self._write_csv(csv_name, self.pose_list)     
        self.pose_list = []

    def add_waypoint(self, pose:Float64MultiArray, msg = "Added waypoint number"):
        self.pose_list.append(list(pose.data))
        if msg is not None:
            print("\n" + msg, len(self.pose_list), "\n")

    def delete_waypoint(self):
        initial_len = cp(len(self.pose_list))
        if initial_len > 0:
            self.pose_list.pop()
            print("\n Removed waypiont number", initial_len, " Now we have", initial_len-1, "waypoints")
        else:
            print("\n Cannot delete, already 0 waypoints :( ")


class TeachWholeMotion(BaseTeachingFunctions):

    def __init__(self) -> None:
        super().__init__()
        self.command_list = []
        self.num_of_saved_files = 0

    # Protected
    def _save_command_json(self):
        json_object = json.dumps({self.dir_name:self.command_list}, indent=4)
        with open(self.parent_dir + self.dir_name + "/" + self.dir_name + ".json", "w") as outfile:
            outfile.write(json_object)

    # Public
    def makedir(self, dir_name:str):
        self._set_dir_name(dir_name)

        target = self.parent_dir + self.dir_name
        print(target)
        try:
            os.mkdir(target)
        except:
            prompt_msg = "\nThe directory '" + dir_name + "' already exists do you want to continue?(y/n)"
            if y_n_prompt(prompt_msg):
                shutil.rmtree(target)
                os.mkdir(target)
            else:
                sys.exit()

    def write_arm_waypoints_to_file(self):

        csv_name = self.dir_name + "_" + str(self.num_of_saved_files)

        command_number = len(self.command_list) + 1
        self.command_list.append({"command number": command_number,"type":"arm", "csv_name":csv_name})

        self._save_command_json()
        self._save_waypoint_csv(csv_name)

        self.num_of_saved_files += 1
        self.pose_list = []

    def write_hand_motion_to_file(self, file_name, reverse):
        command_number = len(self.command_list) + 1
        self.command_list.append({"command number": command_number,"type":"hand", 
                                  "csv_name":file_name, "reverse":reverse})
        self._save_command_json()


class TeachHand(BaseTeachingFunctions):
    def __init__(self) -> None:
        super().__init__()
        self._set_dir_name("hand")

    def write_hand_waypoints_to_file(self, csv_name):
        self._save_waypoint_csv(csv_name)