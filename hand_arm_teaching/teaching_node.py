import rclpy
from rclpy.node import Node
from std_msgs.msg import Float64MultiArray
from sensor_msgs.msg import Joy
from rcl_interfaces.msg import ParameterDescriptor
from .teaching_functions import TeachWholeMotion
from .playback_functions import PlaybackHand
from helper_functions.gamepad_functions import GamepadFunctions
from helper_functions.utility import y_n_prompt

class TeachingNode(Node):

    def __init__(self):
        super().__init__('teaching_node')

        self.gp = GamepadFunctions()
        self.teach = TeachWholeMotion()
        self.play_hand = PlaybackHand()

        self.franka_euler_pose = None
        self.tendon_position = None  
        self.first_wp = True

        # Parameters
        param_descriptor = ParameterDescriptor(description='Folder name for the trajectory')
        self.declare_parameter('dir_name', 'test', param_descriptor)

        # Publisher
        self.franka_stiffness_demand_publisher = self.create_publisher(Float64MultiArray, '/franka/stiffness_demand', 10)
        self.tendon_pos_publisher = self.create_publisher(Float64MultiArray, '/tendon_position_demand', 10)

        # Subscriber
        self.mainloop = self.create_timer(0.001, self.mainloop_callback) 
        self.gamepad_subscriber = self.create_subscription(Joy, '/gamepad', self.gamepad_callback, 10)
        self.franka_euler_pose_subscriber = self.create_subscription(Float64MultiArray, '/franka/euler_pose', self.franka_euler_pose_callback, 10)
        self.tendon_position_subscriber = self.create_subscription(Float64MultiArray, '/hand_controller/tendon_positions', self.tendon_position_callback, 10)

        # Things to call at the beginning of the node creation
        self.dir_name = self.get_parameter('dir_name').value
        self.teach.makedir(self.dir_name)

    def print_menu(self):
        print("\n---\nPress 'right': disengage stiffness\nPress 'left': engage stiffness\nPress 'up': add wp\nPress 'down': delete wp\nPress 'circle': save to file\nPress 'square': teach hand\n---\n")

    def franka_euler_pose_callback(self, franka_euler_pose:Float64MultiArray):
        self.franka_euler_pose = franka_euler_pose

    def gamepad_callback(self, gamepad_raw_msg:Joy):
        self.gp.convert_joy_msg_to_dictionary(gamepad_raw_msg)

    def tendon_position_callback(self, tendon_position:Float64MultiArray):
        self.tendon_position = tendon_position

    def mainloop_callback(self):

        if (self.franka_euler_pose is not None) and (self.tendon_position is not None):
            # self.get_logger().info("Code is running!", throttle_duration_sec = 2)

            if self.first_wp:
                self.teach.add_waypoint(self.franka_euler_pose)
                self.first_wp = False
                self.print_menu()

            if self.gp.if_button_pressed("right"):
                self.get_logger().info("\nSetting stiffness to zero\n", throttle_duration_sec = 0.01)

                demand_stiffness = Float64MultiArray()
                demand_stiffness.data = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
                self.franka_stiffness_demand_publisher.publish(demand_stiffness)
                self.print_menu()

            if self.gp.if_button_pressed("left"):
                self.get_logger().info("\nSetting stiffness back to default\n", throttle_duration_sec = 0.01)
                
                demand_stiffness = Float64MultiArray()
                demand_stiffness.data = [400.0, 400.0, 400.0, 30.0, 30.0, 30.0, 0.0]
                self.franka_stiffness_demand_publisher.publish(demand_stiffness)
                self.print_menu()
                
            if self.gp.if_button_pressed("up"):
                self.teach.add_waypoint(self.franka_euler_pose)
            
            if self.gp.if_button_pressed("down"):
                self.teach.delete_waypoint()

            if self.gp.if_button_pressed("circle"):
                self.get_logger().info("\nSaving waypoint file", throttle_duration_sec = 0.01)                
                self.teach.write_arm_waypoints_to_file()  
                self.teach.add_waypoint(self.franka_euler_pose) 
                self.print_menu()

            if self.gp.if_button_pressed("square"):
                contents = self.play_hand.list_and_select_file_name()
                if contents is None:
                    print("No file was selected")
                else:
                    self.teach.write_hand_motion_to_file(contents[0], contents[1])

                    if y_n_prompt("Play selected hand motion? (y/n)"):
                        self.play_hand.playback_hand(contents[0], self.tendon_position, self.tendon_pos_publisher, contents[1])

                self.print_menu()
            
def main(args=None):
    rclpy.init(args=args)

    teaching_node = TeachingNode()

    rclpy.spin(teaching_node)

    teaching_node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()